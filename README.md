# What does HideMyAss do? #

HideMyAss VPN does what most VPNs do, which is to secure your connection when using the internet. You can’t be too careful when using the internet in modern times, so instead of having to worry about prying eyes, many people are turning to VPNs.

This VPN allows users to change their location to one of over 350 locations worldwide. By changing your location, your original IP address is no longer visible. There are two major reasons that one might want to do this. One is security and the other utility.
 
Using a VPN for utility means that you change your location to gain access to certain areas of the internet that were previously limited. 

This mainly occurs when you want to watch or download online content unavailable in your present geographical location. Netflix US is one example of this, where users outside the US cannot use their service. However, by changing your location to look like you are in the US, HideMyAss will allow you to gain access to all of your favorite sites. 

HideMyAss is reliable and they offer a large pool of servers/protocols to choose from. In addition, you know that the company behind the VPN, Avast, is reliable and one that has established its name in the online security industry.
 
However, there are a few drawbacks including a logging policy and minimal P2P torrent downloading availability.  

Want to compare more VPNs? Check out [https://privacyonline.fi/](https://privacyonline.fi/)https://privacyonline.fi/. 
